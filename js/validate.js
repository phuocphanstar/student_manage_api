function showMessage(idSpan,message){
    document.getElementById(idSpan).innerText=message;
}
function kiemTraTrung(id){
    var index=dssv.findIndex(function (item){
        return item.ma==id;
    });
    if (index ==-1){
        showMessage("spanMaSV","");
        return true;
    }
    else {
        showMessage("spanMaSV","Mã SV đã bị trùng");
    }
}
function kiemTraDoDai(id,message,min,max,value){
    var length=value.length;
    if(length>=min && length <=max){
        showMessage(id,"");
        return true;
    }
    else{
        showMessage(id,message);
        return false;
    }
}
function kiemTraEmail(id,value){
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if(re.test(value)){
    showMessage(id,"");
    return true; 
  }
  else 
  {
    showMessage(id,"Sai định dạng email");
    return false;
  }

}