var idUpdate = null;
//lấy dssv từ api và render 
function fetchStudentList(){
    batLoading();
    svService
    .getList()
    .then((res) => {
            renderDSSV(res.data);
            tatLoading();
          })
    .catch((err) => {
           console.log(err);
           tatLoading();
          });
}
fetchStudentList();

function themSinhVien() {
  var sv = layThongTinTuForm();
  console.log(sv);
  console.log(sv.password);
  //validate phần mật khẩu
  var isValid = kiemTraDoDai("spanMatKhau","Mật khẩu gồm 10 kí tự",10,10,sv.password);
  console.log(isValid);
  //validate phần email
  isValid=isValid&kiemTraEmail("spanEmailSV",sv.email);
    if(isValid){
    batLoading();
    svService
    .create(sv)
    .then((res) => {
            fetchStudentList();
            tatLoading();
          })
          .catch((err) => {
           console.log(err);
           tatLoading();
          });

}
}
function xoaSinhVien(id){
    batLoading();
    svService
    .delete(id)
    .then((res) => {
            console.log(res);
            fetchStudentList();
            tatLoading();
          })
          .catch((err) => {
           console.log(err);
          });
}
function suaSinhVien(id){
    idUpdate=id;
    batLoading();
   svService
   .getById(id)
   .then((res) => {
           showThongTinLenForm(res.data);
           tatLoading();

         })
         .catch((err) => {
          tatLoading();
         });
    
}
function capNhatSinhVien(){
    batLoading();
    var sv = layThongTinTuForm();
    svService
    .update(idUpdate,sv)
    .then((res) => {
            fetchStudentList();
            tatLoading();
          })
          .catch((err) => {
            tatLoading();
          });
   
}
function reSetForm(){
    document.getElementById("formQLSV").reset();
}