function renderDSSV(ArrSV) {
  var contentHTML = "";
  ArrSV.forEach(function(item){
    var sv =new SinhVien(item.name,item.email,item.password,item.math,item.physic,item.chemistry);
    var content =`<tr>
    <td>${item.id}</td>
    <td>${sv.name}</td>
    <td>${sv.email}</td>
    <td>${sv.aveRa()}</td>
    <td>
      <button class="btn btn-danger" onclick="xoaSinhVien('${item.id}')">Xoá</button>
      <button class="btn btn-warning" onclick="suaSinhVien('${item.id}')">Sửa</button>
      </td>
    </tr>`
    contentHTML+=content;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
 
function showThongTinLenForm(sv){
     document.getElementById("txtMaSV").value=sv.id;
     document.getElementById("txtTenSV").value=sv.name;
     document.getElementById("txtEmail").value=sv.email;
     document.getElementById("txtPass").value=sv.password;
     document.getElementById("txtDiemToan").value=sv.math;
     document.getElementById("txtDiemLy").value=sv.physic;
    document.getElementById("txtDiemHoa").value=sv.chemistry;
}

function layThongTinTuForm(){
    //Get data from HTML tag
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = Number(document.getElementById("txtDiemToan").value);
  var ly = Number(document.getElementById("txtDiemLy").value);
  var hoa = Number(document.getElementById("txtDiemHoa").value);
  //Create object SV
  return new SinhVien(ten,email,matKhau,toan,ly,hoa);
}
function batLoading(){
  document.getElementById("spinner").style.display="flex";
}
function tatLoading(){
  document.getElementById("spinner").style.display="none";
}
